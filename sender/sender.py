import smtplib

from string import Template

import os
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

sender_address = None
sender_password = None
smtp_server = None
smtp_port = None
user_gitlab_token = None

def get_settings(filename):
    stg = {}
    with open(filename, mode='r', encoding='utf-8') as contacts_file:
        for line in contacts_file:
            key = line.split('=')[0].strip()
            val = line.split('=')[1].strip()
            stg[key] = val
    return stg

def get_contacts(filename):
    """
    Return two lists names, emails containing names and email addresses
    read from a file specified by filename.
    """
    
    names = []
    emails = []
    with open(filename, mode='r', encoding='utf-8') as contacts_file:
        for a_contact in contacts_file:
            names.append(a_contact.split()[0])
            emails.append(a_contact.split()[1])
    return names, emails

def read_template(filename):
    """
    Returns a Template object comprising the contents of the 
    file specified by filename.
    """
    
    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)

def main():
    names, emails = get_contacts('mycontacts.txt') # read contacts
    message_template = read_template('message.txt')
    html_message_template = read_template('html_message.html')

    # set up the SMTP server
    try:
        s = smtplib.SMTP(host=smtp_server, port=smtp_port)
        s.ehlo()
        s.starttls()
        s.login(sender_address, sender_password)
    except:  
        print('Something went wrong...')

    # For each contact, send the email:
    for name, email in zip(names, emails):
        msg = MIMEMultipart()       # create a message

        # add in the actual person name to the message template
        message = message_template.substitute(PERSON_NAME=name.title())
        html_message = html_message_template.substitute(user_gitlab_token=user_gitlab_token)

        # Prints out the message body for our sake
        print(message)

        # setup the parameters of the message
        msg['From']=sender_address
        msg['To']=email
        msg['Subject']="This is TEST"
        
        img_data = open('github.png', 'rb').read()
        image = MIMEImage(img_data, name=os.path.basename('github.png'))
        msg.attach(image)
        # add in the message body
        msg.attach(MIMEText(message, 'plain'))
        # msg.attach(MIMEText(html_message, 'html'))
        
        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg
        
    # Terminate the SMTP session and close the connection
    s.quit()
    
if __name__ == '__main__':
    stg = get_settings('settings.properties')
    sender_address = stg['sender.address']
    sender_password = stg['sender.password']
    smtp_server = stg['smtp.server']
    smtp_port = stg['smtp.port']
    user_gitlab_token = stg['user.gitlab.token']
    main()
