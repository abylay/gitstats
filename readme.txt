### Project for gitlab activity notifications

Enable IMAP for sender email
https://support.google.com/mail/answer/7126229?visit_id=1-636461653093067281-2943298744&rd=2#cantsignin

Install dependencies:
npm install -g phantomjs 
pip install selenium

Create sender/settings.properties file with key-values:
sender.address=
sender.password=
smtp.server=smtp.gmail.com
smtp.port=587
user.gitlab.token=